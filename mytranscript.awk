function red(s) {
    printf "\033[1;31m" s "\033[0m "
}
function green(s) {
    printf "\033[1;32m" s "\033[0m "
}
function blue(s) {
    printf "\033[1;34m" s "\033[0m "
}
BEGIN{
	badcommand=1
	if (STDNAME=="" || GRADE==""){
		printf("Usage: \nawk -f STDNAME=<name-of-student> -v GRADE=<8,9..> -f mytranscript.awk <datafile>\n")
		badcommand=0
		exit 1;
	}
	FS=","
	header="\tSlippery Rock School District"
	header="\n" header "\n"
	header=header "\t\t16057, Slippery Rock"
	header=header "\n\n"
	header=header "Name:" STDNAME "\t Grade:" GRADE
	bar_type="."
	bar_len=45	
	topbar=""
	for(i=0;i<bar_len;i++){
		topbar=topbar bar_type
	}
	botbar=topbar

	colnames=sprintf("%-12s %-10s %-10s %-20s","Course Name", "FM", "Marks Obt.", "%")
	
	tot_fm=0
	tot_mobt=0
	student_found=0
	data_rows=""
}
($1==STDNAME){
	pc=0
	row=sprintf("%-12s %-10s %-10s %-12s\n", $2,$3,$4,pc)

	data_rows=data_rows row
	tot_fm=0
	tot_mobt=0
	student_found=1
}

END{
	if(badcommand==0){
		exit 1;
	}
	if(student_found){
		print green(header)
		system("sleep 1")
		print blue(topbar)
		system("sleep 1")
		print blue(colnames)
		print blue(topbar)
		system("sleep 2")
		print substr(data_rows,0, length(data_rows)-1)	
		system("sleep 1")
		print blue(botbar)
		system("sleep 1")
		agp=0

		result=sprintf("Total FM: %s \nTotal Marks Obtained: %s \nAggregate %: %.2f",tot_fm,tot_mobt,agp);
		print red(result)
		printf("\n\n")	
	}
	else{
		printf("Studnet %s does not exist in the data file\n",STDNAME);
	}
}
