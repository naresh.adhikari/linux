#!/usr/bin/python3
import sys
#pip install -q requests
#pip install -q ratelimit

#run this file as
#cat smhsort.txt | awk '/^rs/{print substr($1,3)}'  | xargs  -I {} ./t.py {} hey


from requests import get, codes as http_code

"""
Then you contact the "frequency by rsid" endpoint refsnp/<rsid>/frequency (docs at our Variation Services page).
"""

rsid=sys.argv[2]
outputfile=sys.argv[1]
#reply=get("https://api.ncbi.nlm.nih.gov/variation/v0/refsnp/{}/frequency".format(rsid)) #16, 1250344562
#print(reply.json())

"""
The reply's contents are of type JSON. So, we use the json method of requests to convert it into a dictionary. You can use that dictionary directly.
"""
#key 1@32117613
#16, 1@11563271'
#rj = reply.json()
#print(type(rj))
#key=list(rj['results'].keys())[0]
#print(key)

#rj['results'][key]['counts']['PRJNA507278']['allele_counts']['SAMN10492695']['C']
#rj['results']['1@32117613']['counts']['PRJNA507278']['allele_counts']['SAMN10492695']['C']

"""
This means that study PRJNA507278 (the dbGaP Allele Frequency Aggregation, ALFA, project) counted 160122 Cs for population SAMN10492695, although the result can be different for new ALFA releases. You can translate those into English with the metadata endpoint. The 1@11563271 is an interval of length 1 starting after nucleotide 11563271 if you number nucleotides starting at 0.
"""
"""
md_json=get("https://api.ncbi.nlm.nih.gov/variation/v0/metadata/frequency").json()
md = {}
for project_json in md_json:
  p = {}
  p['json']=project_json
  p['pops']={}
  md[project_json['bioproject_id']] = p

def add_all_pops(populations, project):
  for p in populations:
    project['pops'][p['biosample_id']] = p
  if 'subs' in p:
    add_all_pops(p['subs'], project)

for prj_id, prj in md.items():
  add_all_pops(prj['json']['populations'], prj)

print(md['PRJNA507278']['json']['short_name'])
print(md['PRJNA507278']['pops']['SAMN10492695']['name'])

"""
"""
So, those were the counts for people with European ancestry from the ALFA project.

However, for programmatic use, we'll want to wrap this in a function because the API is currently limited to one call per second. We can also take care of error conditions.
"""

from requests import get, codes as http_code
from ratelimit import limits
from typing import Any

@limits(calls=1, period=1)  # Only one call per second
def get_frequency_for(rs_id: str) -> Any:
  """
  Retrieve frequency data by rsid in JSON format
  """
  BYRSID_URL = ("https://api.ncbi.nlm.nih.gov/variation/v0/"
                "refsnp/{}/frequency".format(rs_id))

  reply = get(BYRSID_URL)
  if reply.status_code != http_code.ok:
    raise Exception("Request failed: {}\n{}".format(
      reply.status_code, BYRSID_URL))

  content_type = reply.headers['content-type']
  if content_type != 'application/json':
    raise Exception("Unexpected content type: {}\n{}".format(
      content_type, BYRSID_URL))

  return reply.json()
"""
The reply we obtained is represented with a Python dictionary whose element with key results contains the frequency data. That data inside that element is also a Python dictionary. Its elements consist of intervals, each of which is keyed by a combination of its length and its start position.

The elements of each interval are keyed by the id of the study that the frequency data comes from. Inside each study element, the data consists of the reference allele of the interval (ref element) and its frequency counts (counts key).
"""

def print_study_counts(study_id: str, study_counts: Any) -> None:
  """
  Print counts per study

  At present, we only offer counts per allele,
  not yet per genotype
  """
  row=[]
  #print("\tAllele counts for study: {}".format(study_id))
  row+=[str(study_id)]
  allele_counts = study_counts["allele_counts"]
  for pop_id, pop_counts in allele_counts.items():
    #print("\t\tAllele counts for population {}".format(pop_id))
    row+=[str(pop_id)]
    for allele, count in pop_counts.items():
      #print("\t\t\tAllele: {}. Count: {}".format(allele, count))
      row+=[str(allele)]
      row+=[str(count)]

  rowstr=','.join(row)
  rowstr+="\n"
  with open(outputfile,"a") as fo:
     fo.write(rowstr)

"""
In the example code below, we start by retrieving the frequency data for RSID 16. We then iterate over the intervals, and print for each their start and length positions and their ref alelle. Then we iterate over each study and print its allele counts using the function print_study_counts above. Inside that function we can see that the allele counts are broken down first by population and then by allele.
"""
frequency_data = get_frequency_for(rs_id=rsid)
for interval, freq_by_pop in frequency_data["results"].items():
  # Each key describes an interval
  # in <length>@<start> format
  length, start = interval.split("@")
  #print("Start: {}. Length: {}. Ref. Allele: {}".format(start, length, freq_by_pop["ref"]))
  counts_per_study = freq_by_pop["counts"]

  # Print counts per study
  for study_id, study_counts in counts_per_study.items():
    print_study_counts(study_id, study_counts)
